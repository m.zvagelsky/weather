package ru.weather.requesters.http;

public class HttpSenderException extends RuntimeException {
    public HttpSenderException(String message) {
        super(message);
    }
}
