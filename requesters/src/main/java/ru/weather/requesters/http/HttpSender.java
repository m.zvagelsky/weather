package ru.weather.requesters.http;

public interface HttpSender {

    /** Do GET request for specified url, and return response body as a string. */
    String get(String url);
}
