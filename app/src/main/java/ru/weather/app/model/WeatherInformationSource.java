package ru.weather.app.model;

import ru.weather.app.services.source.OpenWeatherMapRequester;
import ru.weather.app.services.source.WeatherApiRequester;

public enum WeatherInformationSource {
    WEATHER_API(WeatherApiRequester.name),
    OPEN_WEATHER_MAP(OpenWeatherMapRequester.name);

    private final String requesterName;

    WeatherInformationSource(String requesterName) {
        this.requesterName = requesterName;
    }

    public String getRequesterName() {
        return requesterName;
    }
}
