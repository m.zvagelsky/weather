package ru.weather.app;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
//@ConfigurationPropertiesScan
public class WeatherApp {
    public static void main(String[] args) {
        new SpringApplicationBuilder().sources(WeatherApp.class).run(args);
    }
}
