package ru.weather.app.services.source;

import ru.weather.app.model.Weather;

import java.time.LocalDate;

public interface WeatherRequester {
    Weather getWeather(String city, LocalDate date);
}
