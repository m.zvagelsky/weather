package ru.weather.app.services.source;

import org.springframework.stereotype.Service;
import ru.weather.app.model.Weather;

import java.time.LocalDate;

@Service(OpenWeatherMapRequester.name) // Spring uses this bean name to build the weather requesters map: name -> bean
public class OpenWeatherMapRequester implements WeatherRequester {
    public static final String name = "open_weather_map";

    @Override
    public Weather getWeather(String city, LocalDate date) {
        return new Weather("11.1");
    }
}
