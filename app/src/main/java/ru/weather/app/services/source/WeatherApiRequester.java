package ru.weather.app.services.source;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.weather.app.config.WeatherAppProperties;
import ru.weather.app.model.Weather;
import ru.weather.requesters.http.HttpSender;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Slf4j
@RequiredArgsConstructor
@Service(WeatherApiRequester.name) // Spring uses this bean name to build the weather requesters map: name -> bean
public class WeatherApiRequester implements WeatherRequester {
    public static final String name = "weather_api";

    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private final WeatherAppProperties config;
    private final HttpSender sender;
    private final ObjectMapper objectMapper;

    @Override
    public Weather getWeather(String city, LocalDate date) {

        var url = String.format("%s/%s/%s", config.getAdapterUrl(), city, dateFormatter.format(date));
        String response = sender.get(url);
        try {
            Weather weather = objectMapper.readValue(response, Weather.class);
            return weather;
        } catch (JsonProcessingException ex) {
            log.error("Fail to parse Weather object from the string: {}, caught an exception: {}",
                    response, ex.getMessage());
            throw new RuntimeException(ex);
        }
    }
}

