package ru.weather.app.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import ru.weather.app.config.JsonConfig;
import ru.weather.app.config.WeatherAppProperties;
import ru.weather.app.model.WeatherInformationSource;
import ru.weather.app.services.WeatherService;
import ru.weather.app.services.source.WeatherApiRequester;
import ru.weather.requesters.http.HttpSender;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(WeatherController.class)
@Import({JsonConfig.class, WeatherAppProperties.class, WeatherService.class, WeatherApiRequester.class})
public class WeatherControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private HttpSender sender;

    @Test
    void getWeatherTest() throws Exception {

        String expectedResponse = "{\"airTemperature\":\"10\"}";

        when(sender.get(anyString()))
                .thenReturn(expectedResponse);

        String url = String.format(
                "/api/v1/weather/%s/London/2022-01-12", WeatherInformationSource.WEATHER_API.name()
        );
        MockHttpServletRequestBuilder request = get(url).accept(MediaType.APPLICATION_JSON_VALUE);
        ResultActions result = mockMvc.perform(request);

        String responseBody = result
                .andExpect(status().isOk())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat(responseBody).isEqualTo(expectedResponse);
    }
}
