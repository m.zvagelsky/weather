package ru.weather.api.adapter.services;

import lombok.RequiredArgsConstructor;
import org.ehcache.Cache;
import org.springframework.stereotype.Service;
import ru.weather.api.adapter.config.AdapterProperties;
import ru.weather.api.adapter.model.Weather;
import ru.weather.api.adapter.parser.ResponseParser;
import ru.weather.requesters.http.HttpSender;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class WeatherApiService {

    private final HttpSender httpSender;
    private final ResponseParser responseParser;
    private final AdapterProperties props;
    /**
     * Map: url -> weather
     */
    private final Cache<String, Weather> weatherCache;
    private final DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

    public Weather getWeather(String city, LocalDate date) {
        var url = String.format(
                "%s?key=%s&q=%s&dt=%s",
                props.getXmlUrl(),
                props.getApiKey(),
                city,
                dateFormatter.format(date));
        Weather cachedWeather = weatherCache.get(url);
        if (cachedWeather != null) {
            return cachedWeather;
        }
        String xml = httpSender.get(url);
        Weather weather = responseParser.parse(xml);
        weatherCache.put(url, weather);
        return responseParser.parse(xml);
    }
}
