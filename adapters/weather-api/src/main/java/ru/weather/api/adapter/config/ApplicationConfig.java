package ru.weather.api.adapter.config;

import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.weather.api.adapter.model.Weather;
import ru.weather.requesters.http.HttpSender;
import ru.weather.requesters.http.JdkSender;

@Configuration
public class ApplicationConfig {

    /**
     * Must have single cache manager.
     */
    private final CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder().build(true);

    @Bean
    public HttpSender httpSender() {
        return new JdkSender();
    }

    @Bean
    public Cache<String, Weather> weatherCache(@Value("${app.cache.size}") int cacheSize) {
        var cacheConfig = CacheConfigurationBuilder
                .newCacheConfigurationBuilder(String.class, Weather.class, ResourcePoolsBuilder.heap(cacheSize))
                .build();
        return cacheManager.createCache("weatherCache", cacheConfig);
    }
}
