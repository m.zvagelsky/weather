package ru.weather.api.adapter.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.weather.api.adapter.model.Weather;
import ru.weather.api.adapter.services.WeatherApiService;

import java.time.LocalDate;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(path = "${app.rest.api.prefix}")
public class WeatherController {

    private final WeatherApiService weatherApiService;

    @GetMapping("/{city}/{date}")
    public  Weather getWeather(
            @PathVariable("city") String city,
            @DateTimeFormat(pattern = "yyyy-MM-dd") @PathVariable("date") LocalDate date
    ) {
        log.info("getWeather for city: {}, and date {}", city, date);
        Weather weather = weatherApiService.getWeather(city, date);
        log.info("weather: {}", weather);
        return weather;
    }
}
